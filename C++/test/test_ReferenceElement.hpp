#ifndef TEST_REFERENCEELEMENT_HPP
#define TEST_REFERENCEELEMENT_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "ReferenceElement.hpp"

using namespace testing;
using namespace std;
using namespace ReferenceElementNamespace;
using namespace Eigen;

namespace ReferenceElementTesting {
TEST(testReferenceElement, testConvexPentagon){

    try
    {
      Vector2d p1(2.5,1);
      Vector2d p2(4,2.1);
      Vector2d p3(3.4,4.2);
      Vector2d p4(1.6,4.2);
      Vector2d p5(1,2.1);
       Vector2d bsx(1,1), adx(4,4.2), asx(1,4.2), bdx(4,1);

      vector<Vector2d> vertices = {p1, p2, p3, p4, p5};
      Polygon polygon(vertices);
      ReferenceElement ref_el(polygon, 1E-8, 1E-4);

       vector<Vector2d> points = ref_el.getBoundingBox();
       vector<Vector2d> expectedpoints = {bsx, p1, bdx, p2, adx, p3, p4, asx, p5};
       for(unsigned int i=0; i<expectedpoints.size(); i++){
           EXPECT_TRUE((abs(points[i](0) - expectedpoints[i](0))<1E-6 && abs(points[i](1) - expectedpoints[i](1))<1E-6));
       }

        Polygon dominio({bsx, Vector2d (7.8, 1), Vector2d(7.8, 11), Vector2d(1, 11)});
        EXPECT_TRUE((ref_el.getMeshArea() - 68 < 1E-6));
        EXPECT_TRUE((ref_el.getReferenceElementArea() - 9.6 < 1E-6));
        EXPECT_TRUE(ref_el.findPointOnPolygon(Vector2d(2.5,1), vertices));
        EXPECT_FALSE(ref_el.findPointOnPolygon(Vector2d(-9,-1), vertices));

      }


    catch (const exception& exception){
          FAIL();
        }
}

TEST(testReferenceElement, testConcaveHexagon){
    try
    {
      Eigen::Vector2d p1(1.5,1);
      Eigen::Vector2d p2(5.6,1.5);
      Eigen::Vector2d p3(5.5,4.8);
      Eigen::Vector2d p4(4.0,6.2);
      Eigen::Vector2d p5(3.2,4.2);
      Eigen::Vector2d p6(1,4);
      Vector2d bsx(1,1), adx(5.6,6.2), asx(1,6.2), bdx(5.6,1);

      vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6};
      Polygon polygon(vertices);
      ReferenceElement ref_el(polygon, 1E-8, 1E-4);

       vector<Vector2d> points = ref_el.getBoundingBox();
       vector<Vector2d> expectedpoints = {bsx, p1, bdx, p2, adx, p4, asx, p6};
       for(unsigned int i=0; i<expectedpoints.size(); i++){
           EXPECT_TRUE((abs(points[i](0) - expectedpoints[i](0))<1E-6 && abs(points[i](1) - expectedpoints[i](1))<1E-6));
       }

       Polygon dominio({bsx, Vector2d (15, 1), Vector2d(15,20), Vector2d(1,20)});
        ref_el.createMesh(dominio);
        EXPECT_TRUE((ref_el.getMeshArea() - 266 < 1E-6));
        EXPECT_TRUE((ref_el.getReferenceElementArea() - 23.92 < 1E-6));
        EXPECT_TRUE(ref_el.findPointOnPolygon(Vector2d(5.6,1.5), vertices));
        EXPECT_FALSE(ref_el.findPointOnPolygon(Vector2d(13,17), vertices));

      }


    catch (const exception& exception){
          FAIL();
        }
}

TEST(testReferenceElement, testConvexHexagon){
    try
    {
        Eigen::Vector2d p1(0.2,0.1);
        Eigen::Vector2d p2(2.4,-1);
        Eigen::Vector2d p3(3.1,0.5);
        Eigen::Vector2d p4(2,2);
        Eigen::Vector2d p5(1.1, 1.4);
        Eigen::Vector2d p6(0.5, 0.8);

      Vector2d bsx(0.2,-1), adx(3.1,2), asx(0.2,2), bdx(3.1,-1);

      vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6};
      Polygon polygon(vertices);
      ReferenceElement ref_el(polygon, 1E-8, 1E-4);

       vector<Vector2d> points = ref_el.getBoundingBox();
       vector<Vector2d> expectedpoints = {bsx, p2, bdx, p3, adx, p4, asx, p1};

      for(unsigned int i=0; i<expectedpoints.size(); i++){
          EXPECT_TRUE((abs(points[i](0) - expectedpoints[i](0))<1E-6 && abs(points[i](1) - expectedpoints[i](1))<1E-6));
       }

        Polygon dominio({bsx, Vector2d (12, -1), Vector2d(12,17), Vector2d(0.2,17)});
        ref_el.createMesh(dominio);
        EXPECT_TRUE((ref_el.getMeshArea() - 212.4 < 1E-6));
        EXPECT_TRUE((ref_el.getReferenceElementArea() - 8.7 < 1E-6));
        EXPECT_TRUE(ref_el.findPointOnPolygon(Vector2d(1.1,1.4), vertices));
        EXPECT_FALSE(ref_el.findPointOnPolygon(Vector2d(7,4.5), vertices));

      }


    catch (const exception& exception){
          FAIL();
        }
}

TEST(testReferenceElement, testConcaveHexagon2){
    try
    {
    Eigen::Vector2d p1(4.06,-1.14);
    Eigen::Vector2d p2(5.64,1.2);
    Eigen::Vector2d p3(3.76,2.34);
    Eigen::Vector2d p4(0.98,1.16);
    Eigen::Vector2d p5(1.94,-0.74);
    Eigen::Vector2d p6(2.9,0.6);


    Vector2d bsx(0.98,-1.14), adx(5.64,2.34), asx(0.98,2.34), bdx(5.64,-1.14);

    vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6};
    Polygon polygon(vertices);
    ReferenceElement ref_el(polygon, 1E-8, 1E-4);

     vector<Vector2d> points = ref_el.getBoundingBox();
     vector<Vector2d> expectedpoints = {bsx, p1, bdx, p2, adx, p3, asx, p4};
    for(unsigned int i=0; i<expectedpoints.size(); i++){
         EXPECT_TRUE((abs(points[i](0) - expectedpoints[i](0))<1E-6 && abs(points[i](1) - expectedpoints[i](1))<1E-6));
     }

      Polygon dominio({bsx, Vector2d (11, -1.14), Vector2d(11,16), Vector2d(0.98,16)});
      ref_el.createMesh(dominio);
      EXPECT_TRUE((ref_el.getMeshArea() - 171.7428 < 1E-6));
      EXPECT_TRUE((ref_el.getReferenceElementArea() - 16.2168 < 1E-6));
      EXPECT_TRUE(ref_el.findPointOnPolygon(Vector2d(3.76,2.34), vertices));
      EXPECT_FALSE(ref_el.findPointOnPolygon(Vector2d(8,1.5), vertices));

    }
    catch (const exception& exception){
          FAIL();
        }
}
}
#endif // TEST_REFERENCEELEMENT_HPP
