#ifndef TEST_CUTTERPOLYGON_HPP
#define TEST_CUTTERPOLYGON_HPP
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "CutterPolygon.hpp"

TEST(TestCutterPolygon, TestCutterRectangle){
    try
    {
      Eigen::Vector2d p1(1,1);
      Eigen::Vector2d p2(5,1);
      Eigen::Vector2d p3(5,3.1);
      Eigen::Vector2d p4(1,3.1);
      Eigen::Vector2d from(2,1.2);
      Eigen::Vector2d to(4,3);
      Eigen::Vector2d int1(1.777778,1);
      Eigen::Vector2d int2(4.11111,3.1);

      double int2X = int2(0), int1X = int1(0), p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), fromX = from(0), toX = to(0);
      double int2Y = int2(1), int1Y = int1(1), p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), fromY = from(1), toY = to(1);

      vector<int> polygonVertices = {0, 1, 2, 3};
      vector<Vector2d> points = {p1, p2, p3, p4};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);

      list<list<int>> expectedcuttedPolygons =  { {4,5,6,7,3,0}, {6,5,4,1,2,7} };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

     EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
     Vector2d point = cut.getPointfromLabel(2);
     EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, int1X, fromX, toX, int2X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, int1Y, fromY, toY, int2Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

     list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);
}

    catch (const exception& exception){
          FAIL();
        }
}

TEST(TestCutterPolygon, TestCutterConvex){
    try
    {
      Eigen::Vector2d p1(2.5,1);
      Eigen::Vector2d p2(4,2.1);
      Eigen::Vector2d p3(3.4,4.2);
      Eigen::Vector2d p4(1.6,4.2);
      Eigen::Vector2d p5(1,2.1);
      Eigen::Vector2d from(1.4,2.75);
      Eigen::Vector2d to(3.6,2.2);
      Eigen::Vector2d int1(1.2,2.8);

      double int1X = int1(0), p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), fromX = from(0), toX = to(0);
      double int1Y = int1(1), p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), fromY = from(1), toY = to(1);

      vector<int> polygonVertices = {0, 1, 2, 3, 4};
      vector<Vector2d> points = {p1, p2, p3, p4, p5};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);

      list<list<int>> expectedcuttedPolygons =  { {1,5,6,7,4,0}, {6,5,1,2,3,7} };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

      EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
      Vector2d point = cut.getPointfromLabel(2);
      EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, toX, fromX, int1X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, toY, fromY, int1Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

       list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);
}

    catch (const exception& exception){
          FAIL();
        }
}

TEST(TestCutterPolygon, TestCutterConcave){

    try
    {
      Eigen::Vector2d p1(1.5,1);
      Eigen::Vector2d p2(5.6,1.5);
      Eigen::Vector2d p3(5.5,4.8);
      Eigen::Vector2d p4(4.0,6.2);
      Eigen::Vector2d p5(3.2,4.2);
      Eigen::Vector2d p6(1,4);
      Eigen::Vector2d from(2,3.7);//10
      Eigen::Vector2d to(4.1,5.9);//7
      Eigen::Vector2d int1(1.1912162,2.8527026);//11
      Eigen::Vector2d int2( 4.2043271,6.009295);//6
      Eigen::Vector2d int_ls1(3.7213116,5.5032787);//8
      Eigen::Vector2d int_ls2(2.4085972,4.1280541);//9

      double int2X = int2(0), int1X = int1(0), p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), p6X = p6(0), fromX = from(0), toX = to(0), int_ls1X = int_ls1(0), int_ls2X = int_ls2(0);
      double int2Y = int2(1), int1Y = int1(1), p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), p6Y = p6(1), fromY = from(1), toY = to(1), int_ls1Y = int_ls1(1), int_ls2Y = int_ls2(1);

      vector<int> polygonVertices = {0, 1, 2, 3, 4, 5};
      vector<Vector2d> points = {p1, p2, p3, p4, p5, p6};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);

      list<list<int>> expectedcuttedPolygons =  { {6,7,8,4,9,10,11,0,1,2}, {7,6,3,8}, {10,9,5,11} };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

      EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
      Vector2d point = cut.getPointfromLabel(2);
      EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, p6X, int2X, toX, int_ls1X, int_ls2X, fromX, int1X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, p6Y, int2Y, toY, int_ls1Y, int_ls2Y, fromY, int1Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

       list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);

}

    catch (const exception& exception){
          FAIL();
        }

}
TEST(TestCutterPolygon, TestCutterConcaveSegmentOnLine){

    try
    {
      Eigen::Vector2d p1(4.06,-1.14);
      Eigen::Vector2d p2(5.64,1.2);
      Eigen::Vector2d p3(3.76,2.34);
      Eigen::Vector2d p4(0.98,1.16);
      Eigen::Vector2d p5(1.94,-0.74);
      Eigen::Vector2d p6(2.9,0.6);
      Eigen::Vector2d from(4.85,-2.325);//fuori dal poligono
      Eigen::Vector2d to(1.7,2.4);//fuori dal poligono
      Eigen::Vector2d int1(2.1855328,1.671701);


      double  int1X = int1(0), p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), p6X = p6(0);
      double  int1Y = int1(1), p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), p6Y = p6(1);

      vector<int> polygonVertices = {0, 1, 2, 3, 4, 5};
      vector<Vector2d> points = {p1, p2, p3, p4, p5, p6};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);

      list<list<int>> expectedcuttedPolygons =  {  {6,5,0,1,2}, {6,3,4,5} };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

      EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
      Vector2d point = cut.getPointfromLabel(2);
      EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, p6X, int1X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, p6Y, int1Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

       list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);


}

    catch (const exception& exception){
          FAIL();
        }
}


TEST(TestCutterPolygon, TestCutterConvexIntersectionOnVertex){

    try
    {
        Eigen::Vector2d p1(0.2,0.1);
        Eigen::Vector2d p2(2.4,-1);
        Eigen::Vector2d p3(3.1,0.5);
        Eigen::Vector2d p4(2,2);
        Eigen::Vector2d p5(1.1, 1.4);
        Eigen::Vector2d p6(0.5, 0.8);
        Eigen::Vector2d from(4,3);
        Eigen::Vector2d to(0,1);


      double  p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), p6X = p6(0);
      double  p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), p6Y = p6(1);

      vector<int> polygonVertices = {0, 1, 2, 3, 4, 5};
      vector<Vector2d> points = {p1, p2, p3, p4, p5, p6};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);

      list<list<int>> expectedcuttedPolygons =  { { 0, 1, 2, 3, 4, 5 } };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

      EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
      Vector2d point = cut.getPointfromLabel(2);
      EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, p6X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, p6Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

       list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);


}

    catch (const exception& exception){
          FAIL();
        }
}
TEST(TestCutterPolygon, Test1DAuriaPolygon)
{
    try{
    Eigen::Vector2d p1(2,-2);
    Eigen::Vector2d p2(0,-1);
    Eigen::Vector2d p3(3,1);
    Eigen::Vector2d p4(0,2);
    Eigen::Vector2d p5(3,2);
    Eigen::Vector2d p6(3,3);
    Eigen::Vector2d p7(-1,3);
    Eigen::Vector2d p8(-3,1);
    Eigen::Vector2d p9(0,0);
    Eigen::Vector2d p10(-3,-2);
    Eigen::Vector2d from(-4,-4);
    Eigen::Vector2d to(4,4);
    Eigen::Vector2d int1(2,2);
    Eigen::Vector2d int2(1.5,1.5);
    Eigen::Vector2d int3(-2,-2);


      double  p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), p6X = p6(0), p7X = p7(0), p8X = p8(0), p9X = p9(0), p10X = p10(0), int1X = int1(0), int2X = int2(0), int3X = int3(0);
      double  p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), p6Y = p6(1), p7Y = p7(1), p8Y = p8(1), p9Y = p9(1), p10Y = p10(1), int1Y = int1(1), int2Y = int2(1), int3Y = int3(1);

      vector<int> polygonVertices = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
      vector<Vector2d> points = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
      CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
      cut.cutPolygon(from, to, polygonVertices, points);
      list<list<int>> expectedcuttedPolygons =  {{ 5, 6, 7, 8, 11, 3, 12 }, { 8, 9, 10 }, { 10, 0, 1, 2, 11 }, { 12, 4, 5 } };
      EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

      EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
      Vector2d point = cut.getPointfromLabel(2);
      EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

      list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, p6X, p7X, p8X, p9X, p10X, int3X, int2X, int1X};
      list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, p6Y, p7Y, p8Y, p9Y, p10Y, int3Y, int2Y, int1Y};
      list<double> newPointsX = cut.getNewPointsX();
      list<double>::iterator it2 = newPointsX.begin();
      for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

      it2 = cut.getNewPointsY().begin();
      for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
          EXPECT_FLOAT_EQ(*it, *it2);
          ++it2;
      }

     list<Vector2d>test_points(points.begin(), points.end());

     bool type = cut.pointPosition(p1, p2, test_points);
     EXPECT_TRUE(type);

     bool type1 = cut.pointPosition(p4, p3, test_points);
     EXPECT_FALSE(type1);


}

    catch (const exception& exception){
          FAIL();
        }
}
TEST(TestCutterPolygon, Test2DAuriaPolygon)
{
    try{
    Eigen::Vector2d p1(2,-2);
    Eigen::Vector2d p2(0,-1);
    Eigen::Vector2d p3(3,1);
    Eigen::Vector2d p4(0,2);
    Eigen::Vector2d p5(3,2);
    Eigen::Vector2d p6(3,3);
    Eigen::Vector2d p7(-1,3);
    Eigen::Vector2d p8(-3,1);
    Eigen::Vector2d p9(0,0);
    Eigen::Vector2d p10(-3,-2);
    Eigen::Vector2d from(0,-3);
    Eigen::Vector2d to(0,4);
    Eigen::Vector2d int1(0,-2);
    Eigen::Vector2d int2(0,3);

    double  p1X = p1(0), p2X = p2(0), p3X = p3(0), p4X = p4(0), p5X = p5(0), p6X = p6(0), p7X = p7(0), p8X = p8(0), p9X = p9(0), p10X = p10(0), int1X = int1(0), int2X = int2(0);
    double  p1Y = p1(1), p2Y = p2(1), p3Y = p3(1), p4Y = p4(1), p5Y = p5(1), p6Y = p6(1), p7Y = p7(1), p8Y = p8(1), p9Y = p9(1), p10Y = p10(1), int1Y = int1(1), int2Y = int2(1);

    vector<int> polygonVertices = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    vector<Vector2d> points = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
    CutterPolygonNamespace::CutterPolygon cut(points, 1E-8, 1E-4);
    cut.cutPolygon(from, to, polygonVertices, points);
    list<list<int>> expectedcuttedPolygons =  {{ 10, 3, 4, 5 }, { 10, 6, 7, 8 }, { 8, 9, 11 }, { 11, 0, 1 }, { 1, 2, 3 } };
    EXPECT_EQ(cut.getCuttedPolygons(), expectedcuttedPolygons);

    EXPECT_EQ(cut.index(p1,cut.getNewPoints()), 0);
    Vector2d point = cut.getPointfromLabel(2);
    EXPECT_TRUE((abs(point(0)-p3(0)) < 1E-6 && abs(point(1)-p3(1)) < 1E-6));

    list<double> pointsX = {p1X, p2X, p3X, p4X, p5X, p6X, p7X, p8X, p9X, p10X, int2X, int1X};
    list<double> pointsY = {p1Y, p2Y, p3Y, p4Y, p5Y, p6Y, p7Y, p8Y, p9Y, p10Y, int2Y,int1Y};
    list<double> newPointsX = cut.getNewPointsX();
    list<double>::iterator it2 = newPointsX.begin();
    for(list<double>::iterator it = pointsX.begin(); it!=pointsX.end(); ++it){
        EXPECT_FLOAT_EQ(*it, *it2);
        ++it2;
    }

    it2 = cut.getNewPointsY().begin();
    for(list<double>::iterator it = pointsY.begin(); it!=pointsY.end(); ++it){
        EXPECT_FLOAT_EQ(*it, *it2);
        ++it2;
    }

   list<Vector2d>test_points(points.begin(), points.end());

   bool type = cut.pointPosition(p1, p2, test_points);
   EXPECT_TRUE(type);

   bool type1 = cut.pointPosition(p4, p3, test_points);
   EXPECT_FALSE(type1);


}

  catch (const exception& exception){
        FAIL();
      }
}

#endif // TEST_CUTTERPOLYGON_HPP
