#ifndef TEST_INTERSECTORLINESEGMENT_H
#define TEST_INTERSECTORLINESEGMENT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "IntersectorLineSegment.hpp"

using namespace testing;
using namespace std;
using namespace IntersectorLineSegmentNamespace;
using namespace Eigen;

namespace  IntersectorLineSegmentTesting{

TEST(TestIntersectorLineSegment, TestIntersection){
    IntersectorLineSegment myIntersectionObj(1E-8, 1E-4);
    Vector2d test_a(1.5, 1.0);
    Vector2d test_b(1.6, 1.5);
    Vector2d test_c(3.2, 4.2);
    Vector2d test_d(1, 4);
    try
    {

      bool type = myIntersectionObj.computeIntersection(test_a, test_b,test_c,test_d);

      
      EXPECT_EQ(myIntersectionObj.getTypeIntersection(), IntersectorLineSegment::Intersection);
      EXPECT_TRUE(type);
      EXPECT_FLOAT_EQ(myIntersectionObj.squaredDistance(test_d,test_c), 4.88);
      EXPECT_FLOAT_EQ(myIntersectionObj.getFirstParametricCoordinate(), 0.49074075);
      EXPECT_FLOAT_EQ(myIntersectionObj.getSecondParametricCoordinate(), 6.2037039);
      EXPECT_FLOAT_EQ(myIntersectionObj.getPointIntersectionX(), 2.12037);
      EXPECT_FLOAT_EQ(myIntersectionObj.getPointIntersectionY(), 4.10185);
    }

    catch (const exception& exception){
          FAIL();
        }
}

TEST(TestIntersectorLineSegment, TestNoIntersection){
    IntersectorLineSegment myIntersectionObj(1E-8, 1E-4);

    Vector2d test_a(1, 0);
    Vector2d test_b(2, -1);
    Vector2d test_c(0, 2);
    Vector2d test_d(0, 4);


    try {

        bool type = myIntersectionObj.computeIntersection(test_a, test_b,test_c,test_d);
         EXPECT_EQ(myIntersectionObj.getTypeIntersection(), IntersectorLineSegment::NoIntersection);
         EXPECT_FALSE(type);
         EXPECT_FLOAT_EQ(myIntersectionObj.squaredDistance(test_d,test_c), 4);

    }

    catch (const exception& exception){
        FAIL();
      }
    try {
        bool type = myIntersectionObj.computeIntersection(test_a, test_b,test_c,test_d);
        myIntersectionObj.getIntersection();
        FAIL();

    }  catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("No intersection between line and segment in computeIntersection or method not used"));
        }

}
TEST(TestIntersectorLineSegment, TestIntersectionSegmentOnLine){
    IntersectorLineSegment myIntersectionObj(1E-8, 1E-4);

    Vector2d test_a(1, 0);
    Vector2d test_b(2, -1);
    Vector2d test_c(0, 1);
    Vector2d test_d(3, -2);

try {


    bool type = myIntersectionObj.computeIntersection(test_a, test_b,test_c,test_d);
    EXPECT_EQ(myIntersectionObj.getTypeIntersection(), IntersectorLineSegment::SegmentOnLine);
    EXPECT_TRUE(type);
    EXPECT_FLOAT_EQ(myIntersectionObj.squaredDistance(test_c,test_d), 18);
}
    catch (const exception& exception){
        FAIL();
    }

    try {

       bool type = myIntersectionObj.computeIntersection(test_a, test_b,test_c,test_d);
       myIntersectionObj.getIntersection();
       FAIL();
    }

catch (const exception& exception) {
        EXPECT_THAT(std::string(exception.what()), Eq("Line and segment in computeIntersection overlap"));
    }

}

}
#endif // TEST_INTERSECTORLINESEGMENT_H

