#ifndef TEST_POLYGON_HPP
#define TEST_POLYGON_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"
#include "Polygon.hpp"

using namespace testing;
using namespace std;
using namespace PolygonNamespace;
using namespace Eigen;

namespace PolygonTesting {

TEST(testPolygon, testConvexPentagon){

    try
    {
      Vector2d p1(2.5,1);
      Vector2d p2(4,2.1);
      Vector2d p3(3.4,4.2);
      Vector2d p4(1.6,4.2);
      Vector2d p5(1,2.1);
      Vector2d bsx(1,1), adx(4,4.2), asx(1,4.2), bdx(4,1);
      vector<Vector2d> vertices = {p1, p2, p3, p4, p5};
      Polygon polygon(vertices);
      vector<Vector2d> vect = polygon.computeBoundingBox();
      vector<Vector2d> boundingbox = {bsx, bdx, adx, asx};
      for(unsigned int i=0; i<vect.size(); i++){
         EXPECT_TRUE((abs(vect[i](0) - boundingbox[i](0)) < 1E-6 && abs(vect[i](1) - boundingbox[i](1)) < 1E-6));
      }

      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE((vertices[i](0) - polygon.getVertices()[i](0) < 1E-6 && vertices[i](1) - polygon.getVertices()[i](1) < 1E-6));
          EXPECT_EQ(i, polygon.getLabels()[i]);
      }

      EXPECT_FALSE(polygon.pointOnBoundingBox(Vector2d(3,4)));
      EXPECT_TRUE(polygon.pointOnBoundingBox(Vector2d(4,4)));
      EXPECT_TRUE((polygon.area() - 6.69 < 1E-6));

      Vector2d direction(2,0);
      Polygon newPolygon = polygon.translation(direction);
      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE(vertices[i](0) - newPolygon.getVertices()[i](0) + direction(0) < 1E-6 &&
                      vertices[i](1) - newPolygon.getVertices()[i](1) + direction(1) < 1E-6);
      }

      Vector2d newpoint(1.5,1.5);
      polygon.push_backVertex(newpoint);
      EXPECT_TRUE((polygon.getVertices().back()(0)-newpoint(0) < 1E-6 &&
                  polygon.getVertices().back()(1)-newpoint(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels().back(), polygon.getVertices().size() - 1);

      Vector2d newpoint2(1,1.3);
      polygon.push_frontVertex(newpoint2);
      EXPECT_TRUE((polygon.getVertices()[0](0)-newpoint2(0) < 1E-6 &&
                  polygon.getVertices()[0](1)-newpoint2(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels()[0],0);

      polygon.clear();
      EXPECT_TRUE(polygon.getVertices().empty() && polygon.getLabels().empty());
    }

    catch (const exception& exception){
          FAIL();
        }
}

TEST(testPolygon, testConcaveHexagon){
    try
    {
      Eigen::Vector2d p1(1.5,1);
      Eigen::Vector2d p2(5.6,1.5);
      Eigen::Vector2d p3(5.5,4.8);
      Eigen::Vector2d p4(4.0,6.2);
      Eigen::Vector2d p5(3.2,4.2);
      Eigen::Vector2d p6(1,4);

      Vector2d bsx(1,1), adx(5.6,6.2), asx(1,6.2), bdx(5.6,1);
      vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6};
      Polygon polygon(vertices);
      vector<Vector2d> vect = polygon.computeBoundingBox();
      vector<Vector2d> boundingbox = {bsx, bdx, adx, asx};
      for(unsigned int i=0; i<vect.size(); i++){
         EXPECT_TRUE((vect[i](0) - boundingbox[i](0) < 1E-6 && vect[i](1) - boundingbox[i](1) < 1E-6));
      }

      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE((vertices[i](0) - polygon.getVertices()[i](0) < 1E-6 && vertices[i](1) - polygon.getVertices()[i](1) < 1E-6));
          EXPECT_EQ(i, polygon.getLabels()[i]);
      }

      EXPECT_FALSE(polygon.pointOnBoundingBox(Vector2d(4,2)));
      EXPECT_TRUE(polygon.pointOnBoundingBox(Vector2d(1,1.5)));
      EXPECT_TRUE((polygon.area() - 15.37 < 1E-6));

      Vector2d direction(1,0);
      Polygon newPolygon = polygon.translation(direction);
      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE(vertices[i](0) - newPolygon.getVertices()[i](0) + direction(0) < 1E-6 &&
                      vertices[i](1) - newPolygon.getVertices()[i](1) + direction(1) < 1E-6);
      }

      Vector2d newpoint(0.5,2);
      polygon.push_backVertex(newpoint);
      EXPECT_TRUE((polygon.getVertices().back()(0)-newpoint(0) < 1E-6 &&
                  polygon.getVertices().back()(1)-newpoint(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels().back(), polygon.getVertices().size() - 1);

      Vector2d newpoint2(0.2,1.2);
      polygon.push_frontVertex(newpoint2);
      EXPECT_TRUE((polygon.getVertices()[0](0)-newpoint2(0) < 1E-6 &&
                  polygon.getVertices()[0](1)-newpoint2(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels()[0],0);

      polygon.clear();
      EXPECT_TRUE(polygon.getVertices().empty() && polygon.getLabels().empty());
    }
    catch (const exception& exception){
          FAIL();
        }
}

TEST(testPolygon, testDAuriaPolygon){
    try
    {
        Eigen::Vector2d p1(2,-2);
        Eigen::Vector2d p2(0,-1);
        Eigen::Vector2d p3(3,1);
        Eigen::Vector2d p4(0,2);
        Eigen::Vector2d p5(3,2);
        Eigen::Vector2d p6(3,3);
        Eigen::Vector2d p7(-1,3);
        Eigen::Vector2d p8(-3,1);
        Eigen::Vector2d p9(0,0);
        Eigen::Vector2d p10(-3,-2);

      Vector2d bsx(-3,-2), adx(3,3), asx(-3,3), bdx(3,-2);
      vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
      Polygon polygon(vertices);
      vector<Vector2d> vect = polygon.computeBoundingBox();
      vector<Vector2d> boundingbox = {bsx, bdx, adx, asx};
      for(unsigned int i=0; i<vect.size(); i++){
         EXPECT_TRUE((vect[i](0) - boundingbox[i](0) < 1E-6 && vect[i](1) - boundingbox[i](1) < 1E-6));
      }

      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE((vertices[i](0) - polygon.getVertices()[i](0) < 1E-6 && vertices[i](1) - polygon.getVertices()[i](1) < 1E-6));
          EXPECT_EQ(i, polygon.getLabels()[i]);
      }

      EXPECT_FALSE(polygon.pointOnBoundingBox(Vector2d(2,2)));
      EXPECT_TRUE(polygon.pointOnBoundingBox(Vector2d(-3,1.5)));
      EXPECT_TRUE((polygon.area() - 17 < 1E-6));

      Vector2d direction(-2.5,0);
      Polygon newPolygon = polygon.translation(direction);
      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE(vertices[i](0) - newPolygon.getVertices()[i](0) + direction(0) < 1E-6 &&
                      vertices[i](1) - newPolygon.getVertices()[i](1) + direction(1) < 1E-6);
      }

      Vector2d newpoint(-2.2,1);
      polygon.push_backVertex(newpoint);
      EXPECT_TRUE((polygon.getVertices().back()(0)-newpoint(0) < 1E-6 &&
                  polygon.getVertices().back()(1)-newpoint(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels().back(), polygon.getVertices().size() - 1);

      Vector2d newpoint2(-3,-0.4);
      polygon.push_frontVertex(newpoint2);
      EXPECT_TRUE((polygon.getVertices()[0](0)-newpoint2(0) < 1E-6 &&
                  polygon.getVertices()[0](1)-newpoint2(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels()[0],0);

      polygon.clear();
      EXPECT_TRUE(polygon.getVertices().empty() && polygon.getLabels().empty());
    }
    catch (const exception& exception){
          FAIL();
        }
}

TEST(testPolygon, testConvexHexagon){
    try
    {
        Eigen::Vector2d p1(0.2,0.1);
        Eigen::Vector2d p2(2.4,-1);
        Eigen::Vector2d p3(3.1,0.5);
        Eigen::Vector2d p4(2,2);
        Eigen::Vector2d p5(1.1, 1.4);
        Eigen::Vector2d p6(0.5, 0.8);

      Vector2d bsx(0.2,-1), adx(3.1,2), asx(0.2,2), bdx(3.1,-1);
      vector<Vector2d> vertices = {p1, p2, p3, p4, p5, p6};
      Polygon polygon(vertices);
      vector<Vector2d> vect = polygon.computeBoundingBox();
      vector<Vector2d> boundingbox = {bsx, bdx, adx, asx};
      for(unsigned int i=0; i<vect.size(); i++){
         EXPECT_TRUE((vect[i](0) - boundingbox[i](0) < 1E-6 && vect[i](1) - boundingbox[i](1) < 1E-6));
      }

      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE((vertices[i](0) - polygon.getVertices()[i](0) < 1E-6 && vertices[i](1) - polygon.getVertices()[i](1) < 1E-6));
          EXPECT_EQ(i, polygon.getLabels()[i]);
      }

      EXPECT_FALSE(polygon.pointOnBoundingBox(Vector2d(1,0.6)));
      EXPECT_TRUE(polygon.pointOnBoundingBox(Vector2d(2,-1)));
      EXPECT_TRUE((polygon.area() - 4.87 < 1E-6));

      Vector2d direction(0,5);
      Polygon newPolygon = polygon.translation(direction);
      for(unsigned int i=0; i<vertices.size(); i++){
          EXPECT_TRUE(vertices[i](0) - newPolygon.getVertices()[i](0) + direction(0) < 1E-6 &&
                      vertices[i](1) - newPolygon.getVertices()[i](1) + direction(1) < 1E-6);
      }

      Vector2d newpoint(0.2,0.3);
      polygon.push_backVertex(newpoint);
      EXPECT_TRUE((polygon.getVertices().back()(0)-newpoint(0) < 1E-6 &&
                  polygon.getVertices().back()(1)-newpoint(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels().back(), polygon.getVertices().size() - 1);

      Vector2d newpoint2(0.4,0.7);
      polygon.push_frontVertex(newpoint2);
      EXPECT_TRUE((polygon.getVertices()[0](0)-newpoint2(0) < 1E-6 &&
                  polygon.getVertices()[0](1)-newpoint2(1) < 1E-6));
      EXPECT_EQ(polygon.getLabels()[0],0);

      polygon.clear();
      EXPECT_TRUE(polygon.getVertices().empty() && polygon.getLabels().empty());
    }
    catch (const exception& exception){
          FAIL();
        }
}

}



#endif // TEST_POLYGON_HPP
