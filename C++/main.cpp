#include <gtest/gtest.h>
#include "Polygon.hpp"
#include "ReferenceElement.hpp"
#include "test_Polygon.hpp"
#include "test_CutterPolygon.hpp"
#include "test_IntersectorLineSegment.hpp"
#include "test_ReferenceElement.hpp"

using namespace PolygonNamespace;
using namespace ReferenceElementNamespace;

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
  Vector2d p1(2.5,1);
  Vector2d p2(4,2.1);
  Vector2d p3(3.4,4.2);
  Vector2d p4(1.6,4.2);
  Vector2d p5(1,2.1);
  Vector2d A(1,1);
  Vector2d B(11,1);
  Vector2d C(11,7.8);
  Vector2d D(1,7.8);
  Polygon polygon({p1, p2, p3, p4, p5}), domain({A, B, C, D});
  ReferenceElement rf(polygon, 1E-8, 1E-6);
  vector<Polygon> mesh = rf.createMesh(domain);
  double meshArea = rf.getMeshArea(), domainArea = domain.area();
  cout << "meshArea = " << meshArea << endl;
  cout << "domainArea = " << domainArea << endl;
  return 0;
}
