#include "Polygon.hpp"

namespace PolygonNamespace {

    Polygon Polygon::translation(const Vector2d& direction)
    {
        vector<Vector2d> newVertices;
        for (unsigned int i = 0; i < this->vertices.size(); i++)
            newVertices.push_back(this->vertices[i] + direction);
        return Polygon(newVertices);
    }
    Polygon::Polygon()
    {

    }

    Polygon::Polygon(const vector<Vector2d>& vertices)
    {
        this->vertices = vertices;
        labels.resize(vertices.size());
        for (int i = 0; i < (int)labels.size(); i++)
            labels[i] = i;
    }

    vector<Vector2d> Polygon::getVertices() const
    {
        return vertices;
    }

    vector<int> Polygon::getLabels() const
    {
        return labels;
    }

    vector<Vector2d> Polygon::getBoundingBox() const
    {
        return boundingBox;
    }

    void Polygon::push_backVertex(const Vector2d& point)
    {
        vertices.push_back(point);
        labels.push_back(vertices.size() - 1);
    }

    void Polygon::push_frontVertex(const Vector2d &point)
    {
        for (unsigned int i = 0; i < labels.size(); i++)
            labels[i]++;
        vertices.insert(vertices.begin(), point);
        labels.insert(labels.begin(), 0);
    }

    void Polygon::clear()
    {
        vertices.clear();
        labels.clear();
    }

    double Polygon::area()
    {
        double sum = 0;
        for (unsigned int i = 0; i < vertices.size(); i++)
        {
            sum += vertices[i](0) * vertices[(i + 1) % vertices.size()](1) -
                    vertices[i](1) * vertices[(i + 1) % vertices.size()](0);
        }
        return fabs(sum)/2;
    }

    vector<Vector2d> Polygon::computeBoundingBox()
    {
        double minX = INT_MAX, minY = INT_MAX, maxX = INT_MIN, maxY = INT_MIN;
        for (unsigned int i = 0; i < vertices.size(); i++)
        {
            if(vertices[i](0) < minX)
                minX = vertices[i](0);
            if(vertices[i](1) < minY)
                minY = vertices[i](1);
            if(vertices[i](0) > maxX)
                maxX = vertices[i](0);
            if(vertices[i](1) > maxY)
                maxY = vertices[i](1);
        }
        Vector2d p1(minX, minY), p2(maxX, minY), p3(maxX, maxY), p4(minX, maxY);
        boundingBox.insert(boundingBox.end(), {p1, p2, p3, p4});
        return boundingBox;
    }

    bool Polygon::pointOnBoundingBox(const Vector2d &point) const
    {
        if (boundingBox.empty())
            return false;
        if (point(0) == boundingBox[0](0) || point(1) == boundingBox[2](1) || point(0) == boundingBox[2](0) || point(1) == boundingBox[0](1))
            return true;
        return false;
    }

}

