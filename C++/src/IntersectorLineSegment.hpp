#ifndef INTERSECTORLINESEGMENT_HPP
#define INTERSECTORLINESEGMENT_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

namespace IntersectorLineSegmentNamespace {

    class IntersectorLineSegment {
        public:
            enum Type {
                NoIntersection = 0,
                SegmentOnLine = 1,
                Intersection = 2,
            };
        private:
            Vector2d pointIntersection;
            double toleranceParallelism, toleranceIntersection;
            Vector2d parametricCoordinates;
            Type typeIntersection;
        public:
            IntersectorLineSegment(const double& toleranceIntersection, const double& toleranceParallelism);
            Vector2d getParametricCoordinates() const;
            double getFirstParametricCoordinate() const;
            double getSecondParametricCoordinate() const;
            double getPointIntersectionX() const;
            double getPointIntersectionY() const;
            Vector2d getIntersection() const;
            Type getTypeIntersection() const;
            double static squaredDistance(const Vector2d& from, const Vector2d& to);
            bool computeIntersection(const Vector2d& from1, const Vector2d& to1, const Vector2d& from2, const Vector2d& to2);
    };

}
#endif // INTERSECTORLINESEGMENT_HPP
