#ifndef CUTTERPOLYGON_HPP
#define CUTTERPOLYGON_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

namespace CutterPolygonNamespace {
    class CutterPolygon {
        private:
            list<list<int>> cuttedPolygons;
            list<Vector2d> newPoints;
            double toleranceIntersection, toleranceParallelism;
            int pointCounter;
        public:
            list<list<int>> getCuttedPolygons() const;
            list<double> getNewPointsX() const;
            list<double> getNewPointsY() const;
            list<Vector2d> getNewPoints() const;
            int index(const Vector2d& point, const list<Vector2d>& points);
            CutterPolygon(const vector<Vector2d>& points, const double& toleranceIntersection, const double& toleranceParallelism);
            bool pointPosition(const Vector2d& p1, const Vector2d& p2, const list<Vector2d>& points);
            void segmentEndsPosition(double& prevParametricCoordinate, const double& thisParametricCoordinate, const Vector2d& from, const Vector2d& to);
            void cutPolygon(const Vector2d& from, const Vector2d& to, const vector<int>& polygonVertices, const vector<Vector2d>& points);
            Vector2d getPointfromLabel(const int& label);
    };
}


#endif // CUTTERPOLYGON_HPP
