#include "CutterPolygon.hpp"
#include "IntersectorLineSegment.hpp"
using namespace IntersectorLineSegmentNamespace;

namespace CutterPolygonNamespace {
    CutterPolygon::CutterPolygon(const vector<Vector2d>& points, const double& toleranceIntersection, const double& toleranceParallelism)
    {
        this->toleranceIntersection = toleranceIntersection;
        this->toleranceParallelism = toleranceParallelism;
        cuttedPolygons.push_back(list<int>());
        pointCounter = points.size();
    }
    list<Vector2d> CutterPolygon::getNewPoints() const
    {
        return newPoints;
    }

    list<double> CutterPolygon::getNewPointsX() const
    {
        list<double> newPointsX;
        for(list<Vector2d>::const_iterator it = newPoints.begin(); it!=newPoints.end(); ++it){
            Vector2d point= *it;
            newPointsX.push_back(point(0));
        }
        return newPointsX;
    }

    list<double> CutterPolygon::getNewPointsY() const
    {
        list<double> newPointsY;
        for(list<Vector2d>::const_iterator it = newPoints.begin(); it!=newPoints.end(); ++it){
            Vector2d point= *it;
            newPointsY.push_back(point(1));
        }
        return newPointsY;
    }

    list<list<int>> CutterPolygon::getCuttedPolygons() const
    {
        return cuttedPolygons;
    }
    int CutterPolygon::index(const Vector2d& point, const list<Vector2d> &points)
    {
        return distance(points.begin(), find(points.begin(), points.end(), point));
    }
    void CutterPolygon::segmentEndsPosition(double &prevParametricCoordinate, const double &thisParametricCoordinate, const Vector2d &from, const Vector2d &to)
    {
        if (thisParametricCoordinate > 1 + toleranceIntersection || thisParametricCoordinate < -toleranceIntersection)
        {
            if (prevParametricCoordinate * thisParametricCoordinate < 0)
            {
                cuttedPolygons.begin()->push_back(pointCounter);
                cuttedPolygons.back().push_front(pointCounter);
                pointCounter++;
                if (thisParametricCoordinate > 1)
                    newPoints.push_back(from);
                else newPoints.push_back(to);
            }
            if (prevParametricCoordinate * thisParametricCoordinate <= 0)
            {
                cuttedPolygons.begin()->push_back(pointCounter);
                cuttedPolygons.back().push_front(pointCounter);
                pointCounter++;
                if (thisParametricCoordinate < 0)
                    newPoints.push_back(from);
                else newPoints.push_back(to);
            }
        }
        else if(prevParametricCoordinate != 0)
        {
            cuttedPolygons.begin()->push_back(pointCounter);
            cuttedPolygons.back().push_front(pointCounter);
            pointCounter++;
            if (prevParametricCoordinate < 0)
                newPoints.push_back(from);
            else newPoints.push_back(to);
        }
        prevParametricCoordinate = 0;
    }
    bool CutterPolygon::pointPosition(const Vector2d &p1, const Vector2d &p2, const list<Vector2d>& points)
    {
        list<Vector2d>::const_iterator it = find(points.begin(), points.end(), p1), at = find(points.begin(), points.end(), p2);
        return (distance(points.begin(), it) < distance(points.begin(), at));
    }
    void CutterPolygon::cutPolygon(const Vector2d &from, const Vector2d &to, const vector<int> &polygonVertices, const vector<Vector2d> &points)
    {
        list<Vector2d> pointsIntersection;
        bool lineSegment = false;
        for (unsigned int i = 0; i < points.size(); i++)
        {
            newPoints.push_back(points[i]);
            IntersectorLineSegment intersector(toleranceIntersection, toleranceParallelism);
            bool intersectionLineSegment = intersector.computeIntersection(from, to, points[i], points[(i + 1) % points.size()]);
            if (intersectionLineSegment)
            {
                if (intersector.getTypeIntersection() == IntersectorLineSegment::SegmentOnLine)
                {
                    pointsIntersection.push_back(points[(i + 1) % points.size()]);
                    lineSegment = true;
                }
                if (intersector.getTypeIntersection() == IntersectorLineSegment::Intersection)
                {
                    if (fabs(intersector.getParametricCoordinates()(0)) <= toleranceIntersection);
                    else if (intersector.getParametricCoordinates()(0) > 1 - toleranceIntersection && intersector.getParametricCoordinates()(0) < 1 + toleranceIntersection)
                        pointsIntersection.push_back(points[(i + 1) % points.size()]);
                    else
                    {
                        newPoints.push_back(intersector.getIntersection());
                        pointsIntersection.push_back(intersector.getIntersection());
                    }
                }
            }
        }
        if ((lineSegment && pointsIntersection.size() == 2) || pointsIntersection.size() < 2)
        {
            cuttedPolygons.begin()->assign(polygonVertices.begin(), polygonVertices.end());
            return;
        }
        list<Vector2d> notSortedPointsIntersection = pointsIntersection;
        list<Vector2d>::iterator it;
        if (fabs(from(0) - to(0)) <= toleranceIntersection)
            pointsIntersection.sort( [](const Vector2d &p1, const Vector2d &p2) {return p1(1) < p2(1);} );
        else pointsIntersection.sort( [](const Vector2d &p1, const Vector2d &p2) {return p1(0) < p2(0);} );
        // decide where the analysis of intersection between line and polygon should begin
        if ((*notSortedPointsIntersection.begin() == *pointsIntersection.begin() && pointsIntersection.back() == notSortedPointsIntersection.back())
           || (!pointPosition(*pointsIntersection.begin(), pointsIntersection.back(), notSortedPointsIntersection) && *pointsIntersection.begin() != notSortedPointsIntersection.back()))
            it = prev(find(newPoints.begin(), newPoints.end(), *pointsIntersection.begin()));
        else it = prev(find(newPoints.begin(), newPoints.end(), pointsIntersection.back()));
        vector<Vector2d>::const_iterator at = find(points.begin(), points.end(), *it);
        unsigned int j = distance(points.begin(), at), max = j + points.size(), pointsSize = points.size();
        Vector2d lineSlope = to - from;
        bool segmentOnLine = false, intersectionOnVertex = false, cut = false;
        double parametricCoordinate = 0;
        newPoints.assign(points.begin(), points.end());
        for (; j < max; j++)
        {
            if (IntersectorLineSegment::squaredDistance(points[j % pointsSize], points[(j + 1) % pointsSize]) > toleranceIntersection * toleranceIntersection)
            {
                bool done = false;
                Matrix2d crossProduct;
                crossProduct.col(0) = lineSlope, crossProduct.col(1) = points[(j + 1) % pointsSize] - points[j % pointsSize];
                double crossProductDet = crossProduct.determinant();
                if (intersectionOnVertex)
                {
                    // check if the line is wrongly oriented
                    Matrix2d checkLine;
                    checkLine.col(0) = lineSlope, checkLine.col(1) = points[(j - 1) % pointsSize] - points[j & pointsSize];
                    if (checkLine.determinant() * checkLine.determinant() > toleranceParallelism * toleranceParallelism * checkLine.col(0).squaredNorm() * checkLine.col(1).squaredNorm() &&
                            ((checkLine.determinant() > 0 && cut) || (checkLine.determinant() < 0 && !cut)))
                        crossProductDet = -crossProductDet;
                }
                IntersectorLineSegment intersector(toleranceIntersection, toleranceParallelism);
                bool intersectionLineSegment = intersector.computeIntersection(from, to, points[j % pointsSize], points[(j + 1) % pointsSize]);
                IntersectorLineSegment::Type typeIntersection = intersector.getTypeIntersection();
                if (segmentOnLine)
                {
                    // check if the line is wrongly oriented
                    if (lineSlope.dot(points[j % pointsSize] - points[(j - 1) % pointsSize]) < 0)
                        crossProductDet = -crossProductDet;
                    if (typeIntersection == IntersectorLineSegment::SegmentOnLine)
                        intersectionLineSegment = false;
                    // in this case there is no clipping
                    else if (crossProductDet > toleranceParallelism)
                    {
                        segmentOnLine = false;
                        intersectionLineSegment = false;
                    }
                    // otherwise a new cut starts here
                    else if (crossProductDet < -toleranceParallelism)
                    {
                        segmentOnLine = false;
                        cut = true;
                        if (intersector.getParametricCoordinates()(1) > 1 + toleranceIntersection || intersector.getParametricCoordinates()(1) < -toleranceIntersection)
                            parametricCoordinate = intersector.getParametricCoordinates()(1);
                        cuttedPolygons.push_back(list<int> ());
                        cuttedPolygons.back().push_back(polygonVertices[j % pointsSize]);
                        cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                        done = true;
                    } 
                    intersectionOnVertex = false;
                }
                if (!cut)
                {
                    if (intersectionOnVertex && typeIntersection != IntersectorLineSegment::SegmentOnLine)
                    {
                        // in this case there is no clipping
                        if (crossProductDet > toleranceParallelism)
                        {
                            intersectionLineSegment = false;
                            intersectionOnVertex = false;
                        }
                        // otherwise a new cut starts here
                        if (crossProductDet < -toleranceParallelism)
                        {
                            intersectionOnVertex = false;
                            cut = true;
                            cuttedPolygons.push_back(list<int> ());
                            cuttedPolygons.back().push_back(polygonVertices[j % pointsSize]);
                            cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                            if (intersector.getParametricCoordinates()(1) > 1 + toleranceIntersection || intersector.getParametricCoordinates()(1) < -toleranceIntersection)
                                parametricCoordinate = intersector.getParametricCoordinates()(1);
                            done = true;
                        }
                    }
                    if (!done)
                    {
                        if (!intersectionLineSegment)
                            cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                        else
                        {
                            if (typeIntersection == IntersectorLineSegment::SegmentOnLine)
                                {
                                    segmentOnLine = true;
                                    cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                                }
                            if (typeIntersection == IntersectorLineSegment::Intersection)
                            {
                                if (intersector.getParametricCoordinates()(0) < 1 + toleranceIntersection && intersector.getParametricCoordinates()(0) > 1 - toleranceIntersection)
                                {
                                    intersectionOnVertex = true;
                                    cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                                }
                                else
                                {
                                    cuttedPolygons.push_back(list<int> ());
                                    newPoints.push_back(intersector.getIntersection());
                                    cuttedPolygons.back().push_back(pointCounter);
                                    cuttedPolygons.begin()->push_back(pointCounter);
                                    cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                                    pointCounter++;
                                    cut = true;
                                    if (intersector.getParametricCoordinates()(1) > 1 + toleranceIntersection || intersector.getParametricCoordinates()(1) < -toleranceIntersection)
                                        parametricCoordinate = intersector.getParametricCoordinates()(1);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (intersectionOnVertex)
                    {
                        intersectionOnVertex = false;
                        if (typeIntersection != IntersectorLineSegment::SegmentOnLine)
                        {
                            // in this case the current cut ends here
                            if (crossProductDet > toleranceParallelism)
                            {
                                cut = false;
                                cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                            }
                            // otherwise a new cut starts here
                            if (crossProductDet < -toleranceParallelism)
                            {
                                cuttedPolygons.push_back(list<int> ());
                                cuttedPolygons.back().push_back(polygonVertices[j % pointsSize]);
                                cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                                if (intersector.getParametricCoordinates()(1) > 1 + toleranceIntersection || intersector.getParametricCoordinates()(1) < -toleranceIntersection)
                                    parametricCoordinate = intersector.getParametricCoordinates()(1);
                            }
                            done = true;
                        }
                        else
                        {
                            segmentOnLine = true;
                            cut = false;
                            cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                        }
                    }
                    if (!done)
                    {
                        if (!intersectionLineSegment)
                            cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                        else
                        {
                            if (typeIntersection == IntersectorLineSegment::Intersection)
                            {
                                segmentEndsPosition(parametricCoordinate, intersector.getParametricCoordinates()(1), from, to);
                                if (intersector.getParametricCoordinates()(0) < 1 + toleranceIntersection && intersector.getParametricCoordinates()(0) > 1 - toleranceIntersection)
                                {
                                    intersectionOnVertex = true;
                                    cuttedPolygons.back().push_back(polygonVertices[(j + 1) % pointsSize]);
                                    cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                                }
                                else
                                {
                                    newPoints.push_back(intersector.getIntersection());
                                    cuttedPolygons.back().push_back(pointCounter);
                                    cuttedPolygons.begin()->push_back(pointCounter);
                                    cuttedPolygons.begin()->push_back(polygonVertices[(j + 1) % pointsSize]);
                                    pointCounter++;
                                    cut = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        // if this boolean is true add a new point to the last polygon and call segmentEndsPosition, because the cut was not finished
        if (cut)
        {
            IntersectorLineSegment intersector(toleranceIntersection, toleranceParallelism);
            intersector.computeIntersection(from, to, points[j % pointsSize], points[(j + 1) % pointsSize]);
            segmentEndsPosition(parametricCoordinate, intersector.getParametricCoordinates()(1), from , to);
            cuttedPolygons.back().push_back(*cuttedPolygons.begin()->begin());
        }
        // vector specialPoints represents all vertices of the polygon which are also intersection points, except the first and the last ones
        vector<pair<int, Vector2d>> specialPoints;
        for (unsigned int i = 0; i < pointsSize; i++)
        {
            list<Vector2d>::iterator it = find(next(pointsIntersection.begin()), prev(pointsIntersection.end()), points[i]);
            if (it != prev(pointsIntersection.end()))
                    specialPoints.emplace_back(i, *it);
        }
        // for each special point the first polygon has to be split in two polygons
        if (!specialPoints.empty() && cut)
        {
            if (fabs(from(0) - to(0)) <= toleranceIntersection)
                sort(specialPoints.begin(), specialPoints.end(), [](const pair<int, Vector2d> p1, const pair<int, Vector2d> p2){return p1.second(1) < p2.second(1);});
            else sort(specialPoints.begin(), specialPoints.end(), [](const pair<int, Vector2d> p1, const pair<int, Vector2d> p2){return p1.second(0) < p2.second(0);});
            for (vector<pair<int, Vector2d>>::iterator it = specialPoints.begin(); it != specialPoints.end(); ++it)
            {
                cuttedPolygons.insert(next(cuttedPolygons.begin()), list<int>(1, it->first));
                list<int>::iterator at = next(find(cuttedPolygons.begin()->begin(), cuttedPolygons.begin()->end(), it->first));
                for (; *at != index(*next(find(pointsIntersection.begin(), pointsIntersection.end(), points[it->first])), newPoints); ++at)
                {
                    next(cuttedPolygons.begin())->push_back(*at);
                    cuttedPolygons.begin()->erase(at);
                }
            }
        }
        if (!specialPoints.empty() && !cut)
        {
            for (list<Vector2d>::iterator it = pointsIntersection.begin(); it != prev(pointsIntersection.end(), 2); ++it)
            {
                list<int>::iterator at = find(cuttedPolygons.begin()->begin(), cuttedPolygons.begin()->end(), index(*it, newPoints));
                ++at;
                if (find(pointsIntersection.begin(), pointsIntersection.end(), *next(newPoints.begin(), *at)) == pointsIntersection.end() && !lineSegment)
                {
                    cuttedPolygons.push_back(list<int>());
                    --at;
                    cuttedPolygons.back().push_back(*at);
                    cuttedPolygons.begin()->erase(at++);
                    for(; find(pointsIntersection.begin(), pointsIntersection.end(), *next(newPoints.begin(), *at)) == pointsIntersection.end(); ++at)
                    {
                        cuttedPolygons.back().push_back(*at);
                        cuttedPolygons.begin()->erase(at);
                    }
                    cuttedPolygons.back().push_back(*at);
                }
                else if(!lineSegment) cuttedPolygons.begin()->erase(prev(at));
            }
        }
    }

    Vector2d CutterPolygon::getPointfromLabel(const int &label)
    {
        return *next(newPoints.begin(), label);
    }
}
