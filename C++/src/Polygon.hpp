#ifndef POLYGON_HPP
#define POLYGON_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;
namespace PolygonNamespace{
    class Polygon {
        private:
            vector<Vector2d> vertices, boundingBox;
            vector<int> labels;
        public:
            Polygon();
            Polygon(const vector<Vector2d>& vertices);
            bool pointInPolygon(const Vector2d& point);
            vector<Vector2d> computeBoundingBox();
            bool pointOnBoundingBox(const Vector2d& point) const;
            double area();
            vector<Vector2d> getVertices() const;
            vector<int> getLabels() const;
            vector<Vector2d> getBoundingBox() const;
            void push_backVertex(const Vector2d& point);
            void push_frontVertex(const Vector2d& point);
            void clear();
            Polygon translation(const Vector2d &direction);
    };
}


#endif // POLYGON_HPP
