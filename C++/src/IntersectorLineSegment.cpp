#include "IntersectorLineSegment.hpp"

namespace IntersectorLineSegmentNamespace {
    IntersectorLineSegment::IntersectorLineSegment(const double &toleranceIntersection, const double& toleranceParallelism)
    {
        this->toleranceParallelism = toleranceParallelism;
        this->toleranceIntersection = toleranceIntersection;
        typeIntersection = NoIntersection;
    }
    IntersectorLineSegment::Type IntersectorLineSegment::getTypeIntersection() const
    {
        return typeIntersection;
    }
    Vector2d IntersectorLineSegment::getIntersection() const
    {
        if (typeIntersection == SegmentOnLine)
            throw runtime_error("Line and segment in computeIntersection overlap");
        if (typeIntersection == NoIntersection)
            throw runtime_error("No intersection between line and segment in computeIntersection or method not used");
        return pointIntersection;
    }
    Vector2d IntersectorLineSegment::getParametricCoordinates() const
    {
        return parametricCoordinates;
    }
    double IntersectorLineSegment::getFirstParametricCoordinate() const
    {
        return parametricCoordinates(0);
    }
    double IntersectorLineSegment::getSecondParametricCoordinate() const
    {
        return parametricCoordinates(1);
    }
    double IntersectorLineSegment::getPointIntersectionX() const
    {
        return pointIntersection(0);
    }
    double IntersectorLineSegment::getPointIntersectionY() const
    {
        return pointIntersection(1);
    }
    double IntersectorLineSegment::squaredDistance(const Vector2d& from, const Vector2d& to)
    {
       return pow(from(0) - to(0), 2) + pow(from(1) - to(1), 2);
    }
    bool IntersectorLineSegment::computeIntersection(const Vector2d& from1, const Vector2d& to1, const Vector2d& from2, const Vector2d& to2)
    {
        Vector2d lineSlope = to1 - from1, edgeSlope = to2 - from2;
        Matrix2d checkParallelism;
        checkParallelism.col(0) = lineSlope, checkParallelism.col(1) = edgeSlope;
        if (checkParallelism.determinant() * checkParallelism.determinant() <= toleranceParallelism * toleranceParallelism * checkParallelism.col(0).squaredNorm() * checkParallelism.col(1).squaredNorm())
        {
            Matrix2d checkSegmentOnLine;
            checkSegmentOnLine.col(0) = from2 - from1, checkSegmentOnLine.col(1) = lineSlope;
            if (checkSegmentOnLine.determinant() * checkSegmentOnLine.determinant() <= toleranceParallelism * toleranceParallelism * checkSegmentOnLine.col(0).squaredNorm() * checkSegmentOnLine.col(1).squaredNorm())
            {
                typeIntersection = SegmentOnLine;
                return true;
            }
            return false;
        }
        else
        {
            Matrix2d matrixTangentVector;
            matrixTangentVector.col(0) = edgeSlope, matrixTangentVector.col(1) = -lineSlope;
            Vector2d rightHandSide = from1 - from2;
            Matrix2d inverseMatrixTangentVector;
            inverseMatrixTangentVector << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0);
            parametricCoordinates = inverseMatrixTangentVector * rightHandSide;
            parametricCoordinates /= matrixTangentVector.determinant();
            if (parametricCoordinates(0) < 1 + toleranceIntersection && parametricCoordinates(0) > -toleranceIntersection)
            {
                pointIntersection = from1 + parametricCoordinates(1) * lineSlope;
                typeIntersection = Intersection;
                return true;
            }
            return false;
        }
    }
}
