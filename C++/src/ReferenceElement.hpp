#ifndef REFERENCEELEMENT_HPP
#define REFERENCEELEMENT_HPP

#include "Polygon.hpp"

using namespace PolygonNamespace;
namespace ReferenceElementNamespace {
    class ReferenceElement
    {
        private:
            vector<Polygon> polygons, mesh;
            vector<Vector2d> boundingBox;
            double toleranceIntersection, toleranceParallelism;
        public:
            ReferenceElement(Polygon& polygon, const double& toleranceIntersection, const double& toleranceParallelism);
            vector<Polygon> getPolygons() const;
            vector<Vector2d> getBoundingBox() const;
            bool findPointOnPolygon(const Vector2d& point, const vector<Vector2d>& points);
            vector<Polygon> createMesh(const Polygon& domain);
            double getReferenceElementArea();
            double getMeshArea();
            vector<Polygon> getMesh() const;
    };
}


#endif // REFERENCEELEMENT_HPP
