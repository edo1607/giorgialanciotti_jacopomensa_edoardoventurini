#include "ReferenceElement.hpp"
#include "CutterPolygon.hpp"

using namespace CutterPolygonNamespace;

namespace ReferenceElementNamespace {

    bool ReferenceElement::findPointOnPolygon(const Vector2d &point, const vector<Vector2d> &points)
    {
        for (unsigned int i = 0; i < points.size(); i++)
            if (fabs(points[i](0) - point(0)) < toleranceIntersection && fabs(points[i](1) - point(1)) < toleranceIntersection)
                return true;
        return false;
    }

    ReferenceElement::ReferenceElement(Polygon& polygon, const double& toleranceIntersecion, const double& toleranceParallelism)
    {
        this->toleranceIntersection = toleranceIntersecion;
        this->toleranceParallelism = toleranceParallelism;
        if (polygon.getBoundingBox().empty())
            boundingBox = polygon.computeBoundingBox();
        else boundingBox = polygon.getBoundingBox();
        polygons.push_back(polygon);
        for (unsigned int i = 0; i < polygon.getVertices().size(); i++)
        {
            if (polygon.pointOnBoundingBox(polygon.getVertices()[i]) &&
                    !findPointOnPolygon(polygon.getVertices()[i], polygon.getBoundingBox()))
            {
                if (polygon.getVertices()[i](0) == polygon.getBoundingBox()[0](0))
                    boundingBox.insert(boundingBox.end(), polygon.getVertices()[i]);
                if (polygon.getVertices()[i](0) == polygon.getBoundingBox()[1](0))
                    boundingBox.insert(find(boundingBox.begin(), boundingBox.end(), polygon.getBoundingBox()[2]), polygon.getVertices()[i]);
                if (polygon.getVertices()[i](1) == polygon.getBoundingBox()[1](1))
                    boundingBox.insert(find(boundingBox.begin(), boundingBox.end(), polygon.getBoundingBox()[1]), polygon.getVertices()[i]);
                if (polygon.getVertices()[i](1) == polygon.getBoundingBox()[2](1))
                    boundingBox.insert(find(boundingBox.begin(), boundingBox.end(), polygon.getBoundingBox()[3]), polygon.getVertices()[i]);
            }
        }
        bool add = false;
        Polygon newPolygon;
        for (int i = 0, j = -1; j < (int)polygon.getVertices().size() + 1; i++)
        {
            if (polygon.pointOnBoundingBox(polygon.getVertices()[i % polygon.getVertices().size()]))
            {
                add = !add;
                if (j == -1)
                    j = 0;
            }
            if (add && j == 0 && (polygon.getVertices()[(i + 1) % polygon.getVertices().size()](0) == polygon.getVertices()[i % polygon.getVertices().size()](0) ||
                                  polygon.getVertices()[(i + 1) % polygon.getVertices().size()](1) == polygon.getVertices()[i % polygon.getVertices().size()](1)))
                 add = false;
            if (!add && j > 0)
            {
                if (!(polygon.getVertices()[(i + 1) % polygon.getVertices().size()](0) == polygon.getVertices()[i % polygon.getVertices().size()](0) ||
                     polygon.getVertices()[(i + 1) % polygon.getVertices().size()](1) == polygon.getVertices()[i % polygon.getVertices().size()](1)))
                    add = true;
                newPolygon.push_backVertex(polygon.getVertices()[i % polygon.getVertices().size()]);
                vector<Vector2d>::iterator it = prev(find(boundingBox.begin(), boundingBox.end(), polygon.getVertices()[i % polygon.getVertices().size()]));
                Vector2d lastPoint = newPolygon.getVertices()[0];
                for (; *it != lastPoint; --it)
                {
                    newPolygon.push_frontVertex(*it);
                    if (it == boundingBox.begin())
                        it = boundingBox.end();
                }
                polygons.push_back(newPolygon);
                newPolygon.clear();
            }
            if (add && j != (int)polygon.getVertices().size())
                newPolygon.push_backVertex(polygon.getVertices()[i % polygon.getVertices().size()]);
            if (j != -1)
                j++;
        }
    }

    vector<Polygon> ReferenceElement::getPolygons() const
    {
        return polygons;
    }

    vector<Vector2d> ReferenceElement::getBoundingBox() const
    {
        return boundingBox;
    }

    vector<Polygon> ReferenceElement::createMesh(const Polygon &domain)
    {
        double domainBase, domainHeight;
        if (domain.getVertices()[0](0) == domain.getVertices()[1](0))
        {
            domainHeight = fabs(domain.getVertices()[1](1) - domain.getVertices()[0](1));
            domainBase = fabs(domain.getVertices()[1](0) - domain.getVertices()[2](0));
        }
        else
        {
            domainBase = fabs(domain.getVertices()[1](0) - domain.getVertices()[0](0));
            domainHeight = fabs(domain.getVertices()[2](1) - domain.getVertices()[1](1));
        }
        double referenceElementBase = polygons[0].getBoundingBox()[1](0) - polygons[0].getBoundingBox()[0](0);
        double referenceElementHeight = polygons[0].getBoundingBox()[2](1) - polygons[0].getBoundingBox()[1](1);
        unsigned int numberOfRightTranslation = domainBase / referenceElementBase;
        unsigned int numberOfUpTranslation = domainHeight / referenceElementHeight;
        for (unsigned int k = 0; k < numberOfUpTranslation; k++)
            for (unsigned int i = 0; i < numberOfRightTranslation; i++)
                for (unsigned int j = 0; j < polygons.size(); j++)
                    mesh.push_back(polygons[j].translation(Vector2d(i * referenceElementBase, k * referenceElementHeight)));
        vector<Polygon> newPolygons;
        for (unsigned int i = 0; i < polygons.size(); i++)
        {
            Polygon polygon = polygons[i].translation(Vector2d(numberOfRightTranslation * referenceElementBase, 0));
            CutterPolygon cutter(polygon.getVertices(), toleranceIntersection, toleranceParallelism);
            cutter.cutPolygon(domain.getVertices()[1], domain.getVertices()[2], polygon.getLabels(), polygon.getVertices());
            list<list<int>> cuttedPolygons = cutter.getCuttedPolygons();
            for (list<list<int>>::iterator it = cuttedPolygons.begin(); it != cuttedPolygons.end(); ++it)
            {
                list<int>::iterator at;
                Polygon cuttedPolygon;
                for (at = it->begin(); at != it->end(); ++at)
                {
                    if (cutter.getPointfromLabel(*at)(0) > domain.getVertices()[1](0))
                        break;
                    cuttedPolygon.push_backVertex(cutter.getPointfromLabel(*at));
                }
                if (at == it->end())
                {
                    mesh.push_back(cuttedPolygon);
                    newPolygons.push_back(cuttedPolygon);
                }
            }
        }
        for (unsigned int i = 1; i < numberOfUpTranslation; i++)
            for (unsigned int j = 0; j < newPolygons.size(); j++)
                mesh.push_back(newPolygons[j].translation(Vector2d(0, i * referenceElementHeight)));
        newPolygons.clear();
        for (unsigned int i = 0; i < polygons.size(); i++)
        {
            Polygon polygon = polygons[i].translation(Vector2d(0, numberOfUpTranslation * referenceElementHeight));
            CutterPolygon cutter(polygon.getVertices(), toleranceIntersection, toleranceParallelism);
            cutter.cutPolygon(domain.getVertices()[2], domain.getVertices()[3], polygon.getLabels(), polygon.getVertices());
            list<list<int>> cuttedPolygons = cutter.getCuttedPolygons();
            for (list<list<int>>::iterator it = cuttedPolygons.begin(); it != cuttedPolygons.end(); ++it)
            {
                list<int>::iterator at;
                Polygon cuttedPolygon;
                for (at = it->begin(); at != it->end(); ++at)
                {
                    if (cutter.getPointfromLabel(*at)(1) > domain.getVertices()[2](1))
                        break;
                    cuttedPolygon.push_backVertex(cutter.getPointfromLabel(*at));
                }
                if (at == it->end())
                {
                    mesh.push_back(cuttedPolygon);
                    newPolygons.push_back(cuttedPolygon);
                }
            }
        }
        for (unsigned int i = 1; i < numberOfRightTranslation; i++)
            for (unsigned int j = 0; j < newPolygons.size(); j++)
                mesh.push_back(newPolygons[j].translation(Vector2d(i * referenceElementBase, 0)));
        newPolygons.clear();
        for (unsigned int i = 0; i < polygons.size(); i++)
        {
            Polygon polygon = polygons[i].translation(Vector2d(numberOfRightTranslation * referenceElementBase, numberOfUpTranslation * referenceElementHeight));
            CutterPolygon cutter(polygon.getVertices(), toleranceIntersection, toleranceParallelism);
            cutter.cutPolygon(domain.getVertices()[2], domain.getVertices()[3], polygon.getLabels(), polygon.getVertices());
            list<list<int>> cuttedPolygons = cutter.getCuttedPolygons();
            for (list<list<int>>::iterator it = cuttedPolygons.begin(); it != cuttedPolygons.end(); ++it)
            {
                list<int>::iterator at;
                Polygon cuttedPolygon;
                for (at = it->begin(); at != it->end(); ++at)
                {
                    if (cutter.getPointfromLabel(*at)(1) > domain.getVertices()[2](1))
                        break;
                    cuttedPolygon.push_backVertex(cutter.getPointfromLabel(*at));
                }
                if (at == it->end())
                    newPolygons.push_back(cuttedPolygon);
            }
        }
        for (unsigned int i = 0; i < newPolygons.size(); i++)
        {
            CutterPolygon cutter(newPolygons[i].getVertices(), toleranceIntersection, toleranceParallelism);
            cutter.cutPolygon(domain.getVertices()[1], domain.getVertices()[2], newPolygons[i].getLabels(), newPolygons[i].getVertices());
            list<list<int>> cuttedPolygons = cutter.getCuttedPolygons();
            for (list<list<int>>::iterator it = cuttedPolygons.begin(); it != cuttedPolygons.end(); ++it)
            {
                list<int>::iterator at;
                Polygon cuttedPolygon;
                for (at = it->begin(); at != it->end(); ++at)
                {
                    if (cutter.getPointfromLabel(*at)(0) > domain.getVertices()[2](0))
                        break;
                    cuttedPolygon.push_backVertex(cutter.getPointfromLabel(*at));
                }
                if (at == it->end())
                    mesh.push_back(cuttedPolygon);
            }
        }
        return mesh;
    }

    double ReferenceElement::getReferenceElementArea()
    {
        double area = 0;
        for (unsigned int i = 0; i < polygons.size(); i++)
            area += polygons[i].area();
        return area;
    }

    double ReferenceElement::getMeshArea()
    {
        double area = 0;
        for (unsigned int i = 0; i < mesh.size(); i++)
            area += mesh[i].area();
        return area;
    }

    vector<Polygon> ReferenceElement::getMesh() const
    {
        return mesh;
    }
}

