from unittest import TestCase
import src.CutterPolygon as CutterPolygon
import numpy as np


class TestCutterPolygon(TestCase):
    def test_CutterRectangle(self):
        try:
            p1 = np.array([1, 1])
            p2 = np.array([5, 1])
            p3 = np.array([5, 3.1])
            p4 = np.array([1, 3.1])
            _from = np.array([2, 1.2])
            _to = np.array([4, 3])
            int1 = np.array([1.777778, 1])
            int2 = np.array([4.11111, 3.1])
            polygon_vertices = [0, 1, 2, 3]
            points = [p1, p2, p3, p4]
            cut = CutterPolygon.CutterPolygon(points, 1E-8, 1E-4)
            cut.cutPolygon(_from, _to, polygon_vertices, points)
            expected_cutted_polygons = [[4, 5, 6, 7, 3, 0], [6, 5, 4, 1, 2, 7]]
            self.assertEqual(cut.getCuttedPolygons(), expected_cutted_polygons)
            expected_new_points = [p1, p2, p3, p4, int1, _from, _to, int2]
            self.assertEqual(np.all(cut.getNewPoints()), np.all(expected_new_points))
            points = [point.tolist() for point in points]
            _type = CutterPolygon.pointsPosition(p1.tolist(), p2.tolist(), points)
            self.assertTrue(_type)
            _type1 = CutterPolygon.pointsPosition(p4.tolist(), p3.tolist(), points)
            self.assertFalse(_type1)

        except Exception as ex:
            self.fail()


    def test_CutterConvex(self):
        try:
            p1 = np.array([2.5, 1])
            p2 = np.array([4, 2.1])
            p3 = np.array([3.4, 4.2])
            p4 = np.array([1.6, 4.2])
            p5 = np.array([1, 2.1])
            _from = np.array([1.4, 2.75])
            _to = np.array([3.6, 2.2])
            int1 = np.array([1.2, 2.8])
            int2 = np.array([4, 2.1])
            polygon_vertices = [0, 1, 2, 3, 4]
            points = [p1, p2, p3, p4, p5]
            cut = CutterPolygon.CutterPolygon(points, 1E-8, 1E-4)
            cut.cutPolygon(_from, _to, polygon_vertices, points)
            expected_cutted_polygons = [[1, 5, 6, 7, 4, 0], [6, 5, 1, 2, 3, 7]]
            self.assertEqual(cut.getCuttedPolygons(), expected_cutted_polygons)
            expected_new_points = [p1, p2, p3, p4, p5, _to, _from, int1]
            self.assertEqual(np.all(cut.getNewPoints()), np.all(expected_new_points))
            points = [point.tolist() for point in points]
            _type = CutterPolygon.pointsPosition(p1.tolist(), p2.tolist(), points)
            self.assertTrue(_type)
            _type1 = CutterPolygon.pointsPosition(p4.tolist(), p3.tolist(), points)
            self.assertFalse(_type1)

        except Exception as ex:
            self.fail()

    def test_CutterConcave(self):
        try:
            p1 = np.array([1.5, 1])
            p2 = np.array([5.6, 1.5])
            p3 = np.array([5.5, 4.8])
            p4 = np.array([4.0, 6.2])
            p5 = np.array([3.2, 4.2])
            p6 = np.array([1, 4])
            _from = np.array([2,3.7])
            _to = np.array([4.1, 5.9])
            int1 = np.array([1.1912162, 2.8527026])
            int2 = np.array([4.2043271, 6.009295])
            int_ls1 = np.array([3.7213116, 5.5032787])
            int_ls2 = np.array([2.4085972, 4.1280541])
            polygon_vertices = [0, 1, 2, 3, 4, 5]
            points = [p1, p2, p3, p4, p5, p6]
            cut = CutterPolygon.CutterPolygon(points, 1E-8, 1E-4)
            cut.cutPolygon(_from, _to, polygon_vertices, points)
            expected_cutted_polygons = [[6, 7, 8, 4, 9, 10, 11, 0, 1, 2], [7, 6, 3, 8], [10, 9, 5, 11]]
            self.assertEqual(cut.getCuttedPolygons(), expected_cutted_polygons)
            expected_new_points = [p1, p2, p3, p4, p5, p6, int2, _to, int_ls1, int_ls2, _from, int1]
            self.assertEqual(np.all(cut.getNewPoints()), np.all(expected_new_points))
            points = [point.tolist() for point in points]
            _type = CutterPolygon.pointsPosition(p1.tolist(), p5.tolist(), points)
            self.assertTrue(_type)
            _type1 = CutterPolygon.pointsPosition(p4.tolist(), p2.tolist(), points)
            self.assertFalse(_type1)

        except Exception as ex:
            self.fail()


    def test_CutterConcaveSegmentOnLine(self):
        try:
            p1 = np.array([4.06, -1.14])
            p2 = np.array([5.64, 1.2])
            p3 = np.array([3.76, 2.34])
            p4 = np.array([0.98, 1.16])
            p5 = np.array([1.94, -0.74])
            p6 = np.array([2.9, 0.6])
            _from = np.array([4.85, -2.325])
            _to = np.array([1.7, 2.4])
            int1 = np.array([2.1855328, 1.671701])
            polygon_vertices = [0, 1, 2, 3, 4, 5]
            points = [p1, p2, p3, p4, p5, p6]
            cut = CutterPolygon.CutterPolygon(points, 1E-8, 1E-4)
            cut.cutPolygon(_from, _to, polygon_vertices, points)
            expected_cutted_polygons = [[6, 5, 0, 1, 2], [6, 3, 4, 5]]
            self.assertEqual(cut.getCuttedPolygons(), expected_cutted_polygons)
            expected_new_points = [p1, p2, p3, p4, p5, p6, int1]
            self.assertEqual(np.all(cut.getNewPoints()), np.all(expected_new_points))
            points = [point.tolist() for point in points]
            _type = CutterPolygon.pointsPosition(p3.tolist(), p6.tolist(), points)
            self.assertTrue(_type)
            _type1 = CutterPolygon.pointsPosition(p3.tolist(), p2.tolist(), points)
            self.assertFalse(_type1)

        except Exception as ex:
            self.fail()

    def test_CutterConvexIntersectionOnVertex(self):
        try:
            p1 = np.array([0.2, 0.1])
            p2 = np.array([2.4, -1])
            p3 = np.array([3.1, 0.5])
            p4 = np.array([2, 2])
            p5 = np.array([1.1, 0.8])
            p6 = np.array([1.94, -0.74])
            _from = np.array([4, 3])
            _to = np.array([0, 1])
            polygon_vertices = [0, 1, 2, 3, 4, 5]
            points = [p1, p2, p3, p4, p5, p6]
            cut = CutterPolygon.CutterPolygon(points, 1E-8, 1E-4)
            cut.cutPolygon(_from, _to, polygon_vertices, points)
            expected_cutted_polygons = [[0, 1, 2, 3, 4, 5]]
            self.assertEqual(cut.getCuttedPolygons(), expected_cutted_polygons)
            expected_new_points = [p1, p2, p3, p4, p5, p6]
            self.assertEqual(np.all(cut.getNewPoints()), np.all(expected_new_points))
            points = [point.tolist() for point in points]
            _type = CutterPolygon.pointsPosition(p1.tolist(), p4.tolist(), points)
            self.assertTrue(_type)

            _type1 = CutterPolygon.pointsPosition(p5.tolist(), p1.tolist(), points)
            self.assertFalse(_type1)

        except Exception as ex:
            self.fail()