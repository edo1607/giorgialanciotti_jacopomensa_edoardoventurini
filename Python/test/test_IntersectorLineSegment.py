from unittest import TestCase
import src.IntersectorLineSegment as Intersection
import numpy as np

class TestIntersectorLineSegment(TestCase):
    def test_Intersection(self):
        myIntersectionObj = Intersection.IntersectorLineSegment(1E-8, 1E-4)
        test_a = np.array([1.5, 1])
        test_b = np.array([1.6, 1.5])
        test_c = np.array([3.2, 4.2])
        test_d = np.array([1, 4])

        try:
            _type = myIntersectionObj.computeIntersection(test_a, test_b, test_c, test_d)
            self.assertEqual(myIntersectionObj.getTypeIntersection(), Intersection.Type.INTERSECTION)
            self.assertTrue(_type)
            self.assertTrue(myIntersectionObj.squaredDistance(test_d, test_c) - 4.88 < 1E-6)
            self.assertTrue(myIntersectionObj.getParametricCoordinates()[0] - 0.49074075 < 1E-6)
            self.assertTrue(myIntersectionObj.getParametricCoordinates()[1] - 6.2037039 < 1E-6)
            self.assertTrue(myIntersectionObj.getIntersection()[0] - 2.12037 < 1E-6)
            self.assertTrue(myIntersectionObj.getIntersection()[1] - 4.101851 < 1E-6)

        except Exception as ex:
            self.fail()

    def test_NoIntersection(self):
        myIntersectionObj = Intersection.IntersectorLineSegment(1E-8, 1E-4)
        test_a = np.array([1, 0])
        test_b = np.array([2, -1])
        test_c = np.array([0, 2])
        test_d = np.array([0, 4])

        try:
            _type = myIntersectionObj.computeIntersection(test_a, test_b, test_c, test_d)
            self.assertEqual(myIntersectionObj.getTypeIntersection(), Intersection.Type.NO_INTERSECTION)
            self.assertFalse(_type)
            self.assertEqual(myIntersectionObj.squaredDistance(test_d, test_c), 4)

        except Exception as ex:
            self.fail()

        try:
            _type = myIntersectionObj.computeIntersection(test_a, test_b, test_c, test_d)
            myIntersectionObj.getIntersection()
            self.fail()

        except Exception as ex:
            self.assertEqual(str(ex), "No intersection between line and segment in computeIntersection or method not used")

    def test_IntersectionSegmentOnLine(self):
        myIntersectionObj = Intersection.IntersectorLineSegment(1E-8, 1E-4)
        test_a = np.array([1, 0])
        test_b = np.array([2, -1])
        test_c = np.array([0, 1])
        test_d = np.array([3, -2])

        try:
            _type = myIntersectionObj.computeIntersection(test_a, test_b, test_c, test_d)
            self.assertEqual(myIntersectionObj.getTypeIntersection(), Intersection.Type.SEGMENT_ON_LINE)
            self.assertTrue(_type)
            self.assertEqual(myIntersectionObj.squaredDistance(test_c, test_d), 18)

        except Exception as ex:
            self.fail()

        try:
            _type = myIntersectionObj.computeIntersection(test_a, test_b, test_c, test_d)
            myIntersectionObj.getIntersection()
            self.fail()

        except Exception as ex:
            self.assertEqual(str(ex), "Line and segment in computeIntersection overlap")
