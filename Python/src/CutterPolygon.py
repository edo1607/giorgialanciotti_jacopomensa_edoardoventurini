import numpy as np
from scipy.linalg import det, norm
import src.IntersectorLineSegment as Intersector


def pointsPosition(p1, p2, points):
    return points.index(p1) < points.index(p2)


class CutterPolygon:
    def __init__(self, points: [], toleranceIntersection: float, toleranceParallelism: float):
        self.__toleranceIntersection = toleranceIntersection
        self.__toleranceParallelism = toleranceParallelism
        self.__newPoints = list()
        self.__cuttedPolygons = list()
        self.__cuttedPolygons.append(list())
        self.__pointCounter = len(points)

    def getNewPoints(self):
        return self.__newPoints

    def getCuttedPolygons(self):
        return self.__cuttedPolygons

    def segmentEndsPosition(self, prevParamCoord: float, thisParamCoord: float, From, To):
        if thisParamCoord > 1 + self.__toleranceIntersection or thisParamCoord < -self.__toleranceIntersection:
            if prevParamCoord * thisParamCoord < 0:
                self.__cuttedPolygons[0].append(self.__pointCounter)
                self.__cuttedPolygons[-1].insert(0, self.__pointCounter)
                self.__pointCounter += 1
                if thisParamCoord > 0:
                    self.__newPoints.append(From)
                else:
                    self.__newPoints.append(To)
            if prevParamCoord * thisParamCoord <= 0:
                self.__cuttedPolygons[0].append(self.__pointCounter)
                self.__cuttedPolygons[-1].insert(0, self.__pointCounter)
                self.__pointCounter += 1
                if thisParamCoord < 0:
                    self.__newPoints.append(From)
                else:
                    self.__newPoints.append(To)
        elif prevParamCoord != 0:
            self.__cuttedPolygons[0].append(self.__pointCounter)
            self.__cuttedPolygons[-1].insert(0, self.__pointCounter)
            self.__pointCounter += 1
            if prevParamCoord < 0:
                self.__newPoints.append(From)
            else:
                self.__newPoints.append(To)
        prevParamCoord = 0
        return prevParamCoord

    def cutPolygon(self, From, To, polygonVertices, points):
        pointsIntersection = list()
        lineSegment = False
        for i in range(0, len(points)):
            self.__newPoints.append(points[i])
            intersector = Intersector.IntersectorLineSegment(self.__toleranceIntersection, self.__toleranceParallelism)
            intersectionLineSegment = intersector.computeIntersection(
                From, To, points[i], points[(i + 1) % len(points)])
            typeIntersection = intersector.getTypeIntersection()
            if intersectionLineSegment:
                if typeIntersection == Intersector.Type.SEGMENT_ON_LINE:
                    pointsIntersection.append(points[(i + 1) % len(points)])
                    lineSegment = True
                if typeIntersection == Intersector.Type.INTERSECTION:
                    if abs(intersector.getParametricCoordinates()[0]) <= self.__toleranceIntersection:
                        continue
                    elif 1 - self.__toleranceIntersection < intersector.getParametricCoordinates()[0] < \
                            1 + self.__toleranceIntersection:
                        pointsIntersection.append(points[(i + 1) % len(points)])
                    else:
                        self.__newPoints.append(intersector.getIntersection())
                        pointsIntersection.append(intersector.getIntersection())
        if (lineSegment and len(pointsIntersection) == 2) or len(pointsIntersection) < 2:
            self.__cuttedPolygons[0] = polygonVertices
            return
        notSortedPointsIntersection = pointsIntersection.copy()
        if abs(From[0] - To[0]) <= self.__toleranceIntersection:
            pointsIntersection.sort(key=lambda x: x[1])
        else:
            pointsIntersection.sort(key=lambda x: x[0])
        pointsIntersection = [point.tolist() for point in pointsIntersection]
        notSortedPointsIntersection = [point.tolist() for point in notSortedPointsIntersection]
        self.__newPoints = [point.tolist() for point in self.__newPoints]
        if (np.all(notSortedPointsIntersection[0] == pointsIntersection[0]) and
            np.all(notSortedPointsIntersection[-1] == pointsIntersection[-1])) or \
                (not pointsPosition(pointsIntersection[0], pointsIntersection[-1], notSortedPointsIntersection) and
                 np.all(pointsIntersection[0] != notSortedPointsIntersection[-1])):

            index = self.__newPoints.index(pointsIntersection[0]) - 1
        else:

            index = self.__newPoints.index(pointsIntersection[-1]) - 1
        copyOfPoints = [point.tolist() for point in points]
        index = copyOfPoints.index(self.__newPoints[index])
        Max = index + len(points)
        self.__newPoints.clear()
        self.__newPoints = points
        lineSlope = To - From
        pointsSize = len(points)
        segmentOnLine = False
        intersectionOnVertex = False
        cut = False
        parametricCoordinate: float = 0
        for j in range(index, Max):
            if Intersector.IntersectorLineSegment.squaredDistance(
                    points[j % pointsSize], points[(j + 1) % pointsSize]
            ) > self.__toleranceIntersection * self.__toleranceIntersection:
                done = False
                thisEdgeSlope = points[(j + 1) % pointsSize] - points[j % pointsSize]
                crossProduct = np.array([lineSlope, thisEdgeSlope])
                crossProductDet = det(crossProduct)
                if intersectionOnVertex:
                    prevEdgeSlope = points[(j - 1) % pointsSize] - points[j % pointsSize]
                    checkLine = np.array([lineSlope, prevEdgeSlope])
                    if (det(checkLine) ** 2 > self.__toleranceParallelism ** 2 * norm(lineSlope) ** 2 * norm(
                            thisEdgeSlope) ** 2) and \
                            ((det(checkLine) > 0 and cut) or (det(checkLine) < 0 and not cut)):
                        crossProductDet = -crossProductDet
                intersector = Intersector.IntersectorLineSegment(
                    self.__toleranceIntersection, self.__toleranceParallelism)
                intersectionLineSegment = intersector.computeIntersection(
                    From, To, points[j % pointsSize], points[(j + 1) % pointsSize])
                typeIntersection = intersector.getTypeIntersection()
                if segmentOnLine:
                    if np.inner(lineSlope, points[j % pointsSize] - points[(j - 1) % pointsSize]) < 0:
                        crossProductDet = -crossProductDet
                    if typeIntersection == Intersector.Type.SEGMENT_ON_LINE:
                        intersectionLineSegment = False
                    elif crossProductDet > self.__toleranceParallelism:
                        segmentOnLine = False
                        intersectionLineSegment = False
                    elif crossProductDet < -self.__toleranceParallelism:
                        segmentOnLine = False
                        cut = True
                        if intersector.getParametricCoordinates()[1] > 1 + self.__toleranceIntersection or \
                                intersector.getParametricCoordinates()[1] < -self.__toleranceIntersection:
                            parametricCoordinate = intersector.getParametricCoordinates()[1]
                        self.__cuttedPolygons.append(list())
                        self.__cuttedPolygons[-1].append(polygonVertices[j % pointsSize])
                        self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                        done = True
                    intersectionOnVertex = False
                if not cut:
                    if intersectionOnVertex and typeIntersection != Intersector.Type.SEGMENT_ON_LINE:
                        if crossProductDet > self.__toleranceParallelism:
                            intersectionLineSegment = False
                            intersectionOnVertex = False
                        if crossProductDet < -self.__toleranceParallelism:
                            intersectionOnVertex = False
                            cut = True
                            self.__cuttedPolygons.append(list())
                            self.__cuttedPolygons[-1].append(polygonVertices[j % pointsSize])
                            self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                            if intersector.getParametricCoordinates()[1] > 1 + self.__toleranceIntersection or \
                                    intersector.getParametricCoordinates()[1] < -self.__toleranceIntersection:
                                parametricCoordinate = intersector.getParametricCoordinates()[1]
                            done = True
                    if not done:
                        if not intersectionLineSegment:
                            self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                        else:
                            if typeIntersection == Intersector.Type.SEGMENT_ON_LINE:
                                self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                                segmentOnLine = True
                            if typeIntersection == Intersector.Type.INTERSECTION:
                                if 1 - self.__toleranceIntersection < \
                                        intersector.getParametricCoordinates()[0] < 1 + self.__toleranceIntersection:
                                    intersectionOnVertex = True
                                    self.__cuttedPolygons[0].append(polygonVertices[j + 1])
                                else:
                                    self.__cuttedPolygons.append(list())
                                    self.__newPoints.append(intersector.getIntersection())
                                    self.__cuttedPolygons[-1].append(self.__pointCounter)
                                    self.__cuttedPolygons[0].append(self.__pointCounter)
                                    self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                                    self.__pointCounter += 1
                                    cut = True
                                    if (intersector.getParametricCoordinates()[1] > 1 + self.__toleranceIntersection or
                                            intersector.getParametricCoordinates()[1] < -self.__toleranceIntersection):
                                        parametricCoordinate = intersector.getParametricCoordinates()[1]
                else:
                    if intersectionOnVertex:
                        intersectionOnVertex = False
                        if typeIntersection != Intersector.Type.SEGMENT_ON_LINE:
                            if crossProductDet > self.__toleranceParallelism:
                                self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                                cut = False
                            if crossProductDet < -self.__toleranceParallelism:
                                self.__cuttedPolygons.append(list())
                                self.__cuttedPolygons[-1].append(polygonVertices[j % pointsSize])
                                self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                                if intersector.getParametricCoordinates()[1] > 1 + self.__toleranceIntersection or \
                                        intersector.getParametricCoordinates()[1] < -self.__toleranceIntersection:
                                    parametricCoordinate = intersector.getParametricCoordinates()[1]
                            done = True
                        else:
                            segmentOnLine = True
                            cut = False
                            self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                    if not done:
                        if not intersectionLineSegment:
                            self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                        else:
                            if typeIntersection == Intersector.Type.INTERSECTION:
                                parametricCoordinate = self.segmentEndsPosition(
                                    parametricCoordinate, intersector.getParametricCoordinates()[1], From, To)
                                if 1 - self.__toleranceIntersection < \
                                        intersector.getParametricCoordinates()[0] < 1 + self.__toleranceIntersection:
                                    intersectionOnVertex = True
                                    self.__cuttedPolygons[-1].append(polygonVertices[(j + 1) % pointsSize])
                                    self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                                else:
                                    self.__newPoints.append(intersector.getIntersection())
                                    self.__cuttedPolygons[-1].append(self.__pointCounter)
                                    self.__cuttedPolygons[0].append(self.__pointCounter)
                                    self.__cuttedPolygons[0].append(polygonVertices[(j + 1) % pointsSize])
                                    self.__pointCounter += 1
                                    cut = False
        if cut:
            intersector = Intersector.IntersectorLineSegment(self.__toleranceIntersection, self.__toleranceParallelism)
            intersector.computeIntersection(From, To, points[Max % pointsSize], points[(Max + 1) % pointsSize])
            self.segmentEndsPosition(parametricCoordinate, intersector.getParametricCoordinates()[1], From, To)
            self.__cuttedPolygons[-1].append(self.__cuttedPolygons[0][0])
