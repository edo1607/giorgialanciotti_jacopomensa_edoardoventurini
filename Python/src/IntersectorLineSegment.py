import numpy as np
from numpy.linalg import det, norm
from enum import Enum


class Type(Enum):
    NO_INTERSECTION = 0
    SEGMENT_ON_LINE = 1
    INTERSECTION = 2


class IntersectorLineSegment:
    def __init__(self, toleranceIntersection: float, toleranceParallelism: float):
        self.__toleranceIntersection = toleranceIntersection
        self.__toleranceParallelism = toleranceParallelism
        self.__pointIntersection = np.empty((2, 1))
        self.__typeIntersection = Type.NO_INTERSECTION
        self.__parametricCoordinates = np.empty((2, 1))

    @staticmethod
    def squaredDistance(From, To) -> float:
        return (From[0] - To[0])**2 + (From[1] - To[1])**2

    def getTypeIntersection(self):
        return self.__typeIntersection

    def getParametricCoordinates(self):
        return self.__parametricCoordinates

    def getIntersection(self):
        if self.__typeIntersection == Type.SEGMENT_ON_LINE:
            raise ValueError("Line and segment in computeIntersection overlap")
        if self.__typeIntersection == Type.NO_INTERSECTION:
            raise ValueError("No intersection between line and segment in computeIntersection or method not used")
        return self.__pointIntersection

    def computeIntersection(self, From1, To1, From2, To2) -> bool:
        lineSlope = To1 - From1
        edgeSlope = To2 - From2
        checkParallelism = np.array([lineSlope, edgeSlope])
        if abs(det(checkParallelism)) <= self.__toleranceParallelism * norm(lineSlope) * norm(edgeSlope):
            checkSegmentOnLine = np.array([From2 - From1, lineSlope])
            if abs(det(checkSegmentOnLine)) <= self.__toleranceParallelism * norm(From2 - From1) * norm(lineSlope):
                self.__typeIntersection = Type.SEGMENT_ON_LINE
                return True
            return False
        else:
            matrixTangentVector = np.transpose(np.array([edgeSlope, np.negative(lineSlope)], dtype=float))
            rightHandSide = np.transpose(From1 - From2)
            inverseMatrixTangentVector = np.array([
                [matrixTangentVector[1, 1], -matrixTangentVector[0, 1]], [
                    -matrixTangentVector[1, 0], matrixTangentVector[0, 0]]])
            self.__parametricCoordinates = inverseMatrixTangentVector @ rightHandSide
            self.__parametricCoordinates /= det(matrixTangentVector)
            if -self.__toleranceIntersection < self.__parametricCoordinates[0] < 1 + self.__toleranceIntersection:
                self.__pointIntersection = From1 + self.__parametricCoordinates[1] * lineSlope
                self.__typeIntersection = Type.INTERSECTION
                return True
            return False



